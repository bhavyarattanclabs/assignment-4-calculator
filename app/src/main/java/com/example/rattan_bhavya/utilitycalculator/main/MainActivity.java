package com.example.rattan_bhavya.utilitycalculator.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rattan_bhavya.utilitycalculator.util.AppConstants;
import com.example.rattan_bhavya.utilitycalculator.R;
import com.example.rattan_bhavya.utilitycalculator.entities.Summary;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class MainActivity extends Activity implements AppConstants {

    public static Map<String, LinkedList> summaryMap = new HashMap<>();

    LinkedList<String> addList = new LinkedList<>();
    LinkedList<String> allList = new LinkedList<>();
    LinkedList<String> mulList = new LinkedList<>();
    LinkedList<String> divList = new LinkedList<>();
    LinkedList<String> subList = new LinkedList<>();

    EditText oEtNum1, oEtNum2, oEtResult;
    String operator;
    Double num1, num2, result;
    ImageButton oBtnAdd, oBtnSub, oBtnMul, oBtnDiv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        oEtNum1 = (EditText) findViewById(R.id.etFirstNumber);
        oEtNum2 = (EditText) findViewById(R.id.etSecondNumber);
        oEtResult = (EditText) findViewById(R.id.etResult);
        oBtnAdd = (ImageButton) findViewById(R.id.btnAdd);
        oBtnSub = (ImageButton) findViewById(R.id.btnSubtract);
        oBtnMul = (ImageButton) findViewById(R.id.btnMultiply);
        oBtnDiv = (ImageButton) findViewById(R.id.btnDivide);

        summaryMap.put(ADD, addList);
        summaryMap.put(SUB, subList);
        summaryMap.put(MULTIPLY, mulList);
        summaryMap.put(DIVIDE, divList);
        summaryMap.put(ALL, allList);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAdd:
                operator = ADD;
                validateOperation();
                break;
            case R.id.btnSubtract:
                operator = SUB;
                validateOperation();
                break;
            case R.id.btnMultiply:
                operator = MULTIPLY;
                validateOperation();
                break;
            case R.id.btnDivide:
                operator = DIVIDE;
                validateOperation();
                break;
            case R.id.btnSummary:
                Intent intent = new Intent(this, Summary.class);
                startActivity(intent);
                break;
        }
    }

    public void validateOperation() {
        if (oEtNum1.getText().toString().equals("")) {
            Toast.makeText(this, "Enter First number", Toast.LENGTH_SHORT).show();
            oEtResult.setText("");
        } else if (oEtNum2.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Second number", Toast.LENGTH_SHORT).show();
            oEtResult.setText("");
        } else {
            num1 = Double.parseDouble(oEtNum1.getText().toString());
            num2 = Double.parseDouble(oEtNum2.getText().toString());
            if (operator.equals(ADD)) {
                result = num1 + num2;
                oEtResult.setText("" + result);
                addList.addFirst(num1 + operator + num2 + "=" + result);
                allList.addFirst(num1 + operator + num2 + "=" + result);
            } else if (operator.equals(SUB)) {
                result = num1 - num2;
                oEtResult.setText("" + result);
                subList.addFirst(num1 + operator + num2 + "=" + result);
                allList.addFirst(num1 + operator + num2 + "=" + result);
            } else if (operator.equals(MULTIPLY)) {
                result = num1 * num2;
                oEtResult.setText("" + result);
                mulList.addFirst(num1 + operator + num2 + "=" + result);
                allList.addFirst(num1 + operator + num2 + "=" + result);

            } else if (operator.equals(DIVIDE)) {
                if (num2 != 0) {
                    result = num1 / num2;
                    oEtResult.setText("" + result);
                    allList.addFirst(num1 + operator + num2 + "=" + result);
                    divList.addFirst(num1 + operator + num2 + "=" + result);

                } else
                    Toast.makeText(this, "cannot Divide by ZERO", Toast.LENGTH_SHORT).show();
            }

        }
    }
}

