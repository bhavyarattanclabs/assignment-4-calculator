package com.example.rattan_bhavya.utilitycalculator.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.rattan_bhavya.utilitycalculator.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RATTAN-BHAVYA on 1/29/2015.
 */
public class MyAdapter extends BaseAdapter {

    List<String> summaryList = new ArrayList<>();
    Context ctx;

    public MyAdapter(Context ctx, List<String> summaryList) {
        this.ctx = ctx;
        this.summaryList = summaryList;
    }

    @Override
    public int getCount() {
        return summaryList.size();
    }

    @Override
    public Object getItem(int position) {
        return summaryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.summary_list_row, (ViewGroup) convertView);

        TextView oTvResult = (TextView) view.findViewById(R.id.tvResult);

        oTvResult.setText(summaryList.get(position));
        return view;
    }
}
