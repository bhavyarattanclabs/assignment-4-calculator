package com.example.rattan_bhavya.utilitycalculator.entities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.rattan_bhavya.utilitycalculator.R;
import com.example.rattan_bhavya.utilitycalculator.util.AppConstants;
import com.example.rattan_bhavya.utilitycalculator.util.MyAdapter;

import static com.example.rattan_bhavya.utilitycalculator.main.MainActivity.summaryMap;

import java.util.LinkedList;


public class Summary extends Activity implements AppConstants {

    ListView summaryListView;
    Spinner spinner;
    LinkedList<String> selectedSummaryList;
    MyAdapter adapter;
    //Button clearBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        summaryListView = (ListView) findViewById(R.id.summaryListView);
        spinner = (Spinner) findViewById(R.id.filterSpinner);


        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.my_spinner_item, FILTER_OPTIONS);
        spinner.setAdapter(spinnerAdapter);
        selectedSummaryList = new LinkedList<>();
        selectedSummaryList = (LinkedList<String>) summaryMap.get(ALL).clone();
        adapter = new MyAdapter(this, selectedSummaryList);
        summaryListView.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                if (selectedItem.equals(ADD_STRING)) {
                    click(ADD);
                } else if (selectedItem.equals(SUBTRACT_STRING)) {
                    click(SUB);
                } else if (selectedItem.equals(MULTIPLY_STRING)) {
                    click(MULTIPLY);
                } else if (selectedItem.equals(DIVIDE_STRING)) {
                    click(DIVIDE);
                } else if (selectedItem.equals(ALL)) {
                    click(ALL);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnClear:
                summaryMap.get(ALL).clear();
                summaryMap.get(ADD).clear();
                summaryMap.get(SUB).clear();
                summaryMap.get(MULTIPLY).clear();
                summaryMap.get(DIVIDE).clear();
                selectedSummaryList.clear();
                adapter.notifyDataSetChanged();
                break;
            case R.id.btnBack:
                finish();
                break;
            default:
                break;
        }

    }

    public void click(String required) {
        selectedSummaryList.clear();
        selectedSummaryList = (LinkedList) summaryMap.get(required).clone();
        adapter = new MyAdapter(this, selectedSummaryList);
        summaryListView.setAdapter(adapter);
    }
}
