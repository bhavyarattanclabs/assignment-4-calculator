package com.example.rattan_bhavya.utilitycalculator.util;

/**
 * Created by RATTAN-BHAVYA on 1/29/2015.
 */
public interface AppConstants {
    String ADD = " + ";
    String SUB = " - ";
    String MULTIPLY = " * ";
    String DIVIDE = " / ";
    String ALL = "ALL";

    String ADD_STRING = "ADD";
    String SUBTRACT_STRING = "SUBTRACT";
    String MULTIPLY_STRING = "MULTIPLY";
    String DIVIDE_STRING = "DIVIDE";

    final String[] FILTER_OPTIONS = {"ALL", "ADD", "SUBTRACT", "MULTIPLY", "DIVIDE"};


}
